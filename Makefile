.PHONY: all clean

CC = gcc

BUILDDIR = ./build
SRCDIR = ./src
INCDIR = ./include
OBJECTS = $(patsubst $(SRCDIR)/%.c, $(BUILDDIR)/%.o, $(wildcard $(SRCDIR)/*.c))

MAIN_OBJ = main

INC += -I$(INCDIR)

COMPILE = $(CC) $(INC) -c "$<" -o "$@"
LINK=$(CC) -o "$@"

$(BUILDDIR)/%.o: $(SRCDIR)/%.c
	$(COMPILE)

all: $(MAIN_OBJ)

$(MAIN_OBJ): $(OBJECTS)
	$(LINK) $(OBJECTS)

clean:
	rm -rf $(BUILDDIR)/*
	rm -f $(MAIN_OBJ)